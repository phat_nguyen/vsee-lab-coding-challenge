<?php
include "includes/header.php";
?>
<body>

    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">VSee</a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> Test Computer</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span> An Nguyen</a></li>
                </ul>
            </div>
        </nav>
    </header>



    <!--Start Dashboard Block-->
    <div class='inner-box center-block fadeIn animated' >
        <div class='box-header' ><b>An's Waiting Room</b></div>
        <div class='box-body' data-bind="foreach: patientList" >
            <div class="row patient-queue fadeIn animated" >
                <div class="col-md-2 "><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span></div>
                <div class="col-md-4" ><span data-bind="text: state.PatientName"></span><br /><span data-bind="text: state.PatientReason"></span></div>
                <div class="col-md-3"><span class="glyphicon glyphicon-facetime-video" style="color: green" aria-hidden="true"></span>&nbsp;Online<br />Waiting</div>
                <div class="col-md-3 vs-action"><a href='vsee://' data-bind="attr: { href: 'vsee:'+state.PatientVseeID}" style="" >Chat</a> <a data-bind="attr: { href: 'vsee:'+state.PatientVseeID, vseeid:state.PatientVseeID}, click: callPatient" href="vsee://">Call</a></div> 
            </div>
        </div>
    </div>
    <!--End Dashboard Block-->



    <footer align='center'>&copy; 2016 <a href='#'>VSee</a></footer>
    <script src="http://cdn.pubnub.com/pubnub-3.14.3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>
    <script src="js/common.js"></script>
    <script src="js/app-doctor.js"></script>
</body>
</html>
<?php
