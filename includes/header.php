<!DOCTYPE html>
<html lang="en-us" ng-app="myApp">
    <head>
        <title>Doctor's waiting room</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta charset="UTF-8">

        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/animate.min.css" />
        <style>
            body{
                color: #333;
            }
            .center-text
            {
                text-align: center;
            }
            .navbar-default
            {
                background-color: #3FB143;
                border-bottom: 4px solid #D3ECD4;
            }
            .navbar-default .navbar-brand
            {
                color: #fff;
                font-size: 2em;
            }
            .navbar-default .navbar-nav>li>a
            {
                color: #fff;
            }
            .navbar-default .navbar-brand:hover,
            .navbar-default .navbar-nav>li>a:hover
            {
                color: #fff;
            }
            .inner-box
            {
                max-width: 600px;
                transition: 1s all;
                border: 1px solid #3FB143;
            }
            .inner-box .box-body,
            .inner-box .box-header
            {
                padding: 15px;
            }
            .inner-box .box-header
            {
                background-color: #3FB143;
                color: #fff;
            }
            footer a
            {
                color: #3FB143;
            }
            footer
            {
                margin-top: 35px;
            }
            .patient-queue {
             margin:0px;
             font-size: 0.9em;
                padding-bottom: 10px ;
                padding-top    : 10px ;
                border-bottom: 1px solid #ccc;
            }
            .patient-queue .glyphicon-user
            {
                font-size:2em;
            }
            button 
            {
                background-color: orange;
                color: #fff;
                border: none;
                border-radius: 0;
            }
            .vs-action a
            {
                    background-color: orange;
    color: #fff;padding: 7px 12px;
    border: none;
    border-radius: 0;
            }
        </style>
    </head>