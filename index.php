<?php 
include "includes/header.php";
?>
    <body>

        <header>
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="index.php">VSee</a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> Test Computer</a></li>
                    </ul>
                </div>
            </nav>
        </header>


        <h2 align='center'>Welcome to An's room</h2>
        <p align='center'>If this is an emergency, please call 911</p>

        <!--Start Login Block-->
        <div class='inner-box center-block fadeIn animated' data-bind="css: { show: isUserLogged() == 0, hide: isUserLogged() == 1 }">
            <div class='box-header' ><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <b>Talk to An Nguyen</b></div>
            <div class='box-body' >
                <form class=' ' data-bind="submit: doSubmitForm">

                    <div class="form-group ">
                        <label for="exampleInputName1">Please fill in your name to proceed</label>
                        <input required="required" data-bind='textInput: frmName' type="text" class="form-control" id="exampleInputName1" placeholder="Your Name">
                    </div>
                    <div class="form-group ">
                        <label for="exampleInputVseeId">Please fill in Vsee ID</label>
                        <input required="required" data-bind='textInput: frmVseeID' type="text" class="form-control" id="exampleInputVseeId" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputReason1">Reason for visit (optional)</label>
                        <textarea  data-bind='textInput: frmReason'  class="form-control" id="exampleInputReason1" placeholder="Your reason for visit"></textarea>
                    </div>

                    <button type="submit" class="btn btn-default">Enter Waiting Room</button><br />
                    Room ID: i5v7g
                </form>
            </div>
        </div>
        <!--End Login Block-->

        <!--Start Waiting Block-->
        <div class='inner-box center-block hide fadeIn animated'  data-bind="css: { show: isUserLogged() == 1 }" >
            <div class='box-header' ><span class="glyphicon glyphicon glyphicon-repeat" aria-hidden="true"></span> <b>Connecting with your provider</b></div>
            <div class='box-body' >
                <h3 align="center">Your provider will be with you shortly</h3>
                <div class="center-text"><button  type="submit" class="btn btn-default" data-bind="click: exitRoom">Exit Waiting Room</button></div>
                <hr />
                <p align="center">If you close the video conference by mistake, please <a href='#'>click here to relaunch video</a> again.
                    <br />
                    For any other technical difficulties, please contact support@vsee.com
                </p>
            </div>
        </div>
        <!--End Waiting Block-->

        <footer align='center'>&copy; 2016 <a href='#'>VSee</a></footer>
        <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
        <script src="http://cdn.pubnub.com/pubnub-3.14.3.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js"></script>
        <script src="js/common.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
<?php
