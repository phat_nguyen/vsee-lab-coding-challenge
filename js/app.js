

var pubnubController = [];

pubnubController.pubnub = [];
pubnubController.unSubscribe = function ()
{
    var data =   pubnub.unsubscribe({
            channel : commonObject.channel,
            callback : function(){
                console.log(data);
                viewModel.isUserLogged(0);
            }
        });


};
pubnubController.connect_pubnub = function (vseeid) {
    pubnub = PUBNUB({
            publish_key   : 'pub-c-93d0029a-7467-4439-a210-e845eb84009d',
            subscribe_key : 'sub-c-fc2c1f6c-eb64-11e5-8126-0619f8945a4f',
            uuid: vseeid,
            heartbeat: 120,
            heartbeat_interval: 30  
        });
    console.log( jQuery('form') );
};

pubnubController.where_now = function (vseeid) {
    pubnub.where_now({
            uuid     : vseeid,
            callback : function(m){
                console.log(m);
                if ( m.channels.length > 0 )
                {
                   pubnubController.publish();
                    viewModel.isUserLogged(1);
                }

            },
            error : function(m){
                console.log(m)
            }
        });
};

/**
* Submit form event handler
* @param {string} frmName
* @param {string} frmVseeId
* @param {string} frmReason
*/
pubnubController.publish = function (frmName, frmVseeId, frmReason) {

    pubnub.subscribe({
            channel : commonObject.channel,
            state: {

                'PatientName' : frmName,
                'PatientVseeID' : frmVseeId,
                'PatientReason' : frmReason,
                'PatientStartTime' : new Date().getTime(),

            },
            message : function (message, envelope, channelOrGroup, time, channel) {
                messageJson = (JSON.parse(message));
                console.log(messageJson);
                console.log(messageJson.vseeid);
                console.log(viewModel.frmVseeID());

                if ( messageJson.type == 'call' ) {
                    if ( viewModel.frmVseeID() === messageJson.vseeid )
                    alert('The visit is in progress');
                    else alert('Provider is busy');
                }

                /*console.log(
                "Message Received." + "\n" +
                "Channel or Group: " + JSON.stringify(channelOrGroup) + "\n" +
                "Channel: " + JSON.stringify(channel) + "\n" +
                "Message: " + JSON.stringify(message) + "\n" +
                "Time: " + time + "\n" +
                "Raw Envelope: " + JSON.stringify(envelope)
                )*/
            },
            connect: pub
        })



    function pub() {
        setCookie('cookieVseeId',frmVseeId);
        setCookie('cookieName',frmName);
        setCookie('cookieReason',frmReason);
        viewModel.isUserLogged(1);
    }

};



function AppViewModel() {


    var cookieVseeId = getCookie('cookieVseeId');
    if ( typeof cookieVseeId !== 'undefined' ) {
        pubnubController.connect_pubnub(cookieVseeId);
        // if have cookie of vsee id then check if it's connected to pubnub
        pubnubController.where_now(cookieVseeId);
    }

    /**
    * Trigger exit waiting room
    */
    this.exitRoom =  function() {
        pubnubController.unSubscribe();
    };


    /**
    * Submit form event handler
    * @param {object} formElement
    */
    this.doSubmitForm = function(formElement) {
        this.formElement = formElement;
        pubnubController.connect_pubnub(this.frmVseeID());
        pubnubController.publish(this.frmName(), this.frmVseeID(),  this.frmReason());
    };

    cookieName = getCookie('cookieName');
    cookieVseeId = getCookie('cookieVseeId');
    cookieReason = getCookie('cookieReason');

    if ( cookieName != 'undefined' )
    this.frmName = ko.observable(getCookie('cookieName'));
    else  this.frmName = ko.observable(null);

    if ( cookieVseeId != 'undefined' )
    this.frmVseeID = ko.observable(getCookie('cookieVseeId'));
    else  this.frmVseeID = ko.observable(null);

    if ( cookieReason != 'undefined' )
    this.frmReason = ko.observable(getCookie('cookieReason'));
    else  this.frmReason = ko.observable(null);


    this.isUserLogged = ko.observable(0);

}
// Activates knockout.js
viewModel = new AppViewModel();
ko.applyBindings(viewModel);