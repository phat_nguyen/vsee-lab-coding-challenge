

function AppDoctorViewModel() {

    this.patientList = ko.observable();


}
// Activates knockout.js
doctorViewModel = new AppDoctorViewModel();
ko.applyBindings(doctorViewModel);



pubnub = PUBNUB({
        publish_key   : 'pub-c-93d0029a-7467-4439-a210-e845eb84009d',
        subscribe_key : 'sub-c-fc2c1f6c-eb64-11e5-8126-0619f8945a4f'
    });


pubnub.subscribe({
        channel: commonObject.channel,uuid     : 'doctor',
        presence: function(m){
            console.log(m);
            who_here_now();
        },
        message: function(m){
            console.log(m)
        }
    });

function who_here_now()
{
    pubnub.here_now({
            channel : commonObject.channel,
            state : true,
            callback : function(m){
                console.log(m);
                var  patientList = [];
                for (i = 0; i < m.uuids.length; i++)
                {
                    console.log(m.uuids[i]);
                    if (typeof m.uuids[i].state !== 'undefined')
                    {
                        m.uuids[i].callPatient = (function (e) {

                                pubnub.publish({
                                        channel: commonObject.channel,
                                        message: '{"type":"call","vseeid":"' + e.state.PatientVseeID + '"}',
                                        callback : function(m){
                                            console.log(m)
                                        }
                                    });
                                return true;
                            });
                        patientList.push(m.uuids[i]);
                    }
                } 
                
                patientList.sort(function(a, b){
                    var time1 = parseInt( a.state.PatientStartTime );
                    if ( isNaN(time1) ) time1 = 0;
                    var time2 = parseInt( b.state.PatientStartTime );
                    if ( isNaN(time2) ) time2 = 0;
                    console.log(time2);
                    console.log(time1);
                    return (time1 - time2) 
                    });
                 
                doctorViewModel.patientList(patientList);
            }
        });
}
who_here_now();